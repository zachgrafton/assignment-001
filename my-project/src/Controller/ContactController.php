<?php
/**
 * Created by PhpStorm.
 * User: Zachg
 * Date: 9/23/2018
 * Time: 3:04 AM
 */

namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class ContactController
{

    /**
     * @Route("/")
     */
    public function homepage() {

        // Contact Form:
        // Enter Email:
        // Enter Comment (Limit to 1000 words):

        return new Response('Contact Us:');
    }
}